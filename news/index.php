<?php
require __DIR__ . '/../template/header.php';
require __DIR__ . '/../../dbconf/databaseconnect.php';

$query = $db->prepare("SELECT * FROM news");
$query->execute();
$news = $query->fetchAll();;
?>

<div class="main box-main">
    <div class="row">
        <h2>Наши новости</h2>
        <div class="articles">
            <?foreach ($news as $key => $arItem) {?>
                <div class="news">
                    <h3><?=$arItem['name']?></h3>
                    <p><?=$arItem['detail']?></p>
                    <p>
                        Дата: <span><?=$arItem['date']?></span>
                    </p>
                </div>
            <?}?>
        </div>
        <?php
        include __DIR__ . '/../template/include/link_home.php';
        ?>
    </div>
</div>

<?php
require __DIR__ . '/../template/footer.php';
?>



