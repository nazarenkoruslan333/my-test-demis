<?php
require __DIR__ . '/../../dbconf/databaseconnect.php';

$query = $db->prepare("SELECT * FROM news ORDER BY id DESC LIMIT 3");
$query->execute();
$news = $query->fetchAll();;
?>
<div class="main box-main">
    <div class="row">
        <h2>Наши новости</h2>
        <div class="articles">
            <? foreach ($news as $key => $Items) {?>
                <div class="news">
                    <h3><?=$Items['name']?></h3>
                    <?php
//                    $text = strip_tags($Items['detail']);
                    $text = substr($Items['detail'], 0, 150);
//                    $text = rtrim($Items['detail'], "!,.-");
//                    $text = substr($Items['detail'], 0, strrpos($Items['detail'], ' '));
                    ?>
                    <p><? echo $text."… "; ?></p>
                    <p>
                        Дата: <span><?=$Items['date']?></span>
                    </p>
                </div>
            <?}?>
        </div>
    </div>
</div>