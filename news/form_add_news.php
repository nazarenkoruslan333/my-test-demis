<?php
require __DIR__ . '/../template/header.php';
require __DIR__ . '/../../dbconf/databaseconnect.php';

if (count($_POST) > 0) {
    $date = trim($_POST['dateD']);
    $name = trim($_POST['name']);
    $detail = trim($_POST['detail']);

    $date = htmlspecialchars($date);
    $name = htmlspecialchars($name);
    $detail = htmlspecialchars($detail);

    if ($name != '' && $date != '' && $detail != '') {
        $query = $db->prepare("INSERT INTO news SET date=:date, name=:name, detail=:detail");
        $params = ['date' => $date, 'name' => $name, 'detail' => $detail];
        $query->execute($params);

        header("Location: index.php");
        exit;
    }
}
?>

    <div class="container_form">
        <div class="form__wrapper">
            <legend>
                <h2>
                    Добавить новость
                </h2>
            </legend>
            <form method="POST">
                <div class="form-row news">
                    <input hidden type="datetime-local" name="dateD" value="<?=date('Y-m-d\TH:i:s'); ?>"/>

                    <input id="news_name" name="name" type="text" class="form-control" placeholder="Название новости" required="required"/>

                    <textarea id="news_text" rows="10" cols="45" name="detail" class="form-control" required="required"></textarea>

                </div>

                <input class="form-submit" type="submit" value="Добавить новость"/>

            </form>
        </div>
    </div>

<?php require __DIR__ . '/../template/footer.php';; ?>