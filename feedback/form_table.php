<?php
$query = $db->prepare("SELECT * FROM myform ORDER BY date DESC");
$query->execute();
$rows = $query->fetchAll();
?>

<div class="container_table">
    <table class="table">
        <thead>
        <tr>
            <th>Дата</th>
            <th>ФИО</th>
            <th>Адрес</th>
            <th>Телефон</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $arItem) {?>
            <tr>
                <td><?=$arItem['date']?></td>
                <td><?=$arItem['name']?></td>
                <td><?=$arItem['address']?></td>
                <td><?=$arItem['phone']?></td>
                <td><?=$arItem['email']?></td>
            </tr>
        <?}?>
        </tbody>
    </table>
</div>