<?php
if(count($_POST) > 0){
    $date = trim($_POST['dateC']);
    $name = trim($_POST['name']);
    $address = trim($_POST['address']);
    $phone = trim($_POST['phone']);
    $email = trim($_POST['email']);

    $date = htmlspecialchars($date);
    $name = strip_tags($name);
    $address = htmlspecialchars($address);
    $phone = htmlspecialchars($phone);
    $email = htmlspecialchars($email);

    if ($name != '' && $address != '' && $phone != '' && $email != ''){
        $query = $db->prepare("INSERT INTO myform SET date=:date, name=:name, address=:address, phone=:phone, email=:email");
        $params = ['date' => $date, 'name' => $name, 'address' => $address, 'phone' => $phone, 'email' => $email];
        $query->execute($params);

        header("Location: index.php");
        exit;
    }
}
?>

<div class="container_form">
    <div class="form__wrapper">
            <h2>
                Мы перезвоним Вам
            </h2>
        <form method="POST">
            <div class="form-row">
                <input hidden type="datetime-local" name="dateC" value="<?= date('Y-m-d\TH:i:s'); ?>"/>

                <input id="name" name="name" type="text" placeholder="ФИО" class="form-control" minlength="8" maxlength="50" required="required"/>
                <label for="name" class="control-label">ФИО</label>

                <input id="address" name="address" type="text" placeholder="Адрес" class="form-control" minlength="2"
                       maxlength="300" required="required"/>
                <label for="address" class="control-label">Адрес</label>

                <input id="phone" name="phone" type="tel" placeholder="Телефон" class="form-control"
                       required="required"/>
                <label for="phone" class="control-label">Телефон</label>

                <input id="email" name="email" type="email" placeholder="E-mail" class="form-control"/>
                <label for="email" class="control-label">E-mail</label>
            </div>

            <input class="form-submit" type="submit" value="ОТПРАВИТЬ"/>

            <div class="form-agreement">
                <p>Отправляя данные, Вы соглашаетесь с условиями <a href="https://www.demis.ru/company/terms/">Пользовательского
                        соглашения</a> (Политики конфиденциальности).</p>
            </div>
        </form>
    </div>
</div>